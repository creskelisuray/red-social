<?php
ob_start();
?>
<?php
session_start();
if(isset($_SESSION['logueado']) && $_SESSION['logueado'] == TRUE) {
  header("Location: home.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Imagram</title>
    <link rel="stylesheet" href="css/estilo.css">
</head>

<body>
    <div id="contenedorprincipal">
    <?php
    if (isset($_POST['registro'])){
        require("conexion.php");

        $email = $conexion->real_escape_string($_POST['email']);
        $nombre = $conexion->real_escape_string($_POST['nombre']);
        $usuario = $conexion->real_escape_string($_POST['usuario']);
        $password = md5($_POST['contraseña']);
        $ip = $_SERVER['REMOTE_ADDR'];

        $consultausuario = "SELECT username FROM usuarios WHERE username = '$usuario'";
        $consultaemail = "SELECT email FROM usuarios WHERE email = '$email'";

        if ($resultadousuario = $conexion->query($consultausuario));
        $numerousuario = $resultadousuario->num_rows;

        if ($resultadoemail = $conexion->query($consultaemail));
        $numeroemail = $resultadoemail->num_rows;

        if ($numeroemail > 0) {
            echo "Este correo ya esta registrado, intenta con otro";
        } elseif ($numerousuario > 0) {
            echo "Este usuario ya existe";
        } else {

            $aleatorio = uniqid();
            $query = "INSERT INTO usuarios (email,name,username,password,signup_date,last_ip,code) VALUES ('$email','$nombre','$usuario','$password',now(),'$ip','$aleatorio')";

            if ($registro = $conexion->query($query)) {

                Header("Refresh: 2; URL=index.php");
                echo "Felicidades $usuario se ha registrado correctamente.";

            } else {

                echo "Ha ocurrido un error en el registro, intentelo de nuevo";
                header("Refresh: 2; URL=registro.php");
            }
            }
        $conexion->close();
    }
    ?>
        <div class="form-registro">
            <div class="logo"><img src="imagenes/logo1.png"></div>
            <form action="" method="post">
                <div class="cont-form">
                    <input type="email" placeholder="Correo electrónico" class="input" name="email" required />
                    <div class="cont-datos">
                        <input type="text" placeholder="Nombre completo" class="input" name="nombre" required />
                    </div>
                    <div class="cont-datos">
                        <input type="text" placeholder="Usuario" class="input" name="usuario" required />
                    </div>
                    <div class="cont-datos">
                        <input type="password" placeholder="Contraseña" class="input" name="contraseña" required />
                    </div>
                    <input type="submit" value="Registrarte" class="boton" name="registro" />
                </div>
            </form>
        </div>
        <div class="cont-log">
                ¿Tienes una cuenta? <a href="index.php">Entrar</a>
        </div>
    </div>
</body>
</html>
<?php
ob_end_flush();
?>