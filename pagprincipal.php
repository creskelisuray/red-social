<?php
session_start();
if (!isset($_SESSION['logueado']) && $_SESSION['logueado'] == FALSE) {
    header("Location: index.php");
}
include "funciondusuarios.php";
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilo.css">
    <link href="css/instagram.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.12.0.min.js"></script>
    <script src="js/eliminar.js"></script>
    <title>Imagram</title>
</head>

<body>

    <?php include "cabecerapag.php"; ?>

    <div class="P-p-cont">

        <?php
        require "conexion.php";

        $sqlA = $conexion->query("SELECT * FROM publicaciones ORDER BY idpublicacion DESC");
        while ($rowA = $sqlA->fetch_array()) {

            $sqlB = $conexion->query("SELECT * FROM usuarios WHERE id = '" . $rowA['iduser'] . "'");
            $rowB = $sqlB->fetch_array();

        ?>

            <form action="" method="post">
                <div class="P-cont-izq">
                    <div class="P-cont">
                        <div class="Pa-top">
                            <div class="Paa-perfil">
                                <div class="Pa-foto">
                                    <a href=""><img src="imagenes/<?php echo $rowB['avatar']; ?>"></a>
                                </div>
                            </div>
                            <div class="Pab-usuario">
                                <div class="Pab-nombre">
                                    <a href=""><?php echo $rowB['username']; ?></a>
                                </div>
                            </div>
                            <div class="c-eliminar">
                                <a href="eliminar.php?id=<?php echo $rowA['idpublicacion']; ?>"><input type="button" name="eliminar" id="eliminar" class="Beliminar" value="Eliminar"></a>
                            </div>
                        </div>
                        <div class="Pb-middle">
                            <img src="archivos/<?php echo $rowA['ruta']; ?>" width="100%" class="<?php echo $rowA['filtro']; ?>">
                        </div>
                        <div class="Pc-likes">
                            <div id="" class="like" style="float: left;  cursor: pointer;">
                                <img src="imagenes/iconos/cora.png">
                            </div>

                            <div class="comentarios" style="float: left;">
                                <img src="imagenes/iconos/comentario.png">
                            </div>

                            <div id="" class="fav" style="float: left;">
                                <img src="imagenes/iconos/favorito.png">
                            </div>
                        </div>
                        <div class="Pd-bottom">
                            <strong style="color: #262626;"><?php echo $rowB['username']; ?></strong> <?php echo $rowA['descripcion']; ?>
                        </div>

                    <?php } ?>
                    </div>
                </div>
            </form>

            <div class="P-cont-der">
                <div class="Pa-menu">
                    <div class="Pa-icono"><img src="imagenes/iconos/lupa.png" width="50"></div>
                    <div class="Pa-icono"><a href="subirfoto.php"><img src="imagenes/iconos/mas.png" width="50" title="Sube una foto"></a></div>
                    <div class="Pa-icono"><img src="imagenes/iconos/corazon.png" width="50"></div>
                </div>
                <div class="Pb-top">
                    <div class="Pb-perfil">
                        <div class="Pb-foto"><a href=""><img src="imagenes/<?php datos_usuario($_SESSION['id'], 'avatar'); ?>" width="60" height="60"></a>
                        </div>
                    </div>
                    <div class="Pb-usuario">
                        <div class="Pb-nombre"><a href=""><?php echo $_SESSION['username']; ?></a></div>
                        <div class="Pba-nombre">
                            <?php datos_usuario($_SESSION['id'], 'name'); ?>
                        </div>
                    </div>
                </div>
            </div>
    </div>

</body>

</html>