<?php
ob_start();
?>

<?php
session_start();
if (isset($_SESSION['logueado']) && $_SESSION['logueado'] == TRUE) {
    header("Location: pagprincipal.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/estilo.css">
    <title>Imagram</title>
</head>

<body>
    <div id="contenedorprincipal">
        <div class="cont">
            <?php
            if (isset($_GET['error'])) {
                echo "<center>Error el usuario o contraseña no coinciden</center>";
                header("Refresh: 3; URL=index.php");
            }
            ?>
            <?php
            if (isset($_POST['entrar'])) {
                require("conexion.php");

                $usuario = $conexion->real_escape_string($_POST['usuario']);
                $contraseña = md5($_POST['contraseña']);
                $consulta = "SELECT username,password,id FROM usuarios WHERE username = '$usuario' AND password = '$contraseña'";

                if ($resultado = $conexion->query($consulta)) {
                    while ($row = $resultado->fetch_array()) {

                        $usuok = $row['username'];
                        $contok = $row['password'];
                        $id = $row['id'];
                    }
                    $resultado->close();
                }
                $conexion->close();

                if (isset($usuario) && isset($contraseña)) {

                    if ($usuario == $usuok && $contraseña == $contok) {
                        session_start();
                        $_SESSION['logueado'] = TRUE;
                        $_SESSION['username'] = $usuok;
                        $_SESSION['id'] = $id;
                        header("Location: pagprincipal.php");
                    } else {
                        header("Location: index.php?error=login");
                    }
                }
            }
            ?>
            <div class="cont-login">

                <div class="logo"> <img src="imagenes/logo1.png"></div>

                <div class="formulario-login">
                    <form action="" method="post">

                        <input type="text" placeholder="Usuario" name="usuario" class="input" autocomplete="off" required/>
                        <div class="cont-contraseña">
                            <input type="password" placeholder="Contraseña" name="contraseña" class="input" required/>
                        </div>
                        <input type="submit" value="Iniciar sesion" name="entrar" class="boton">
                        <div class="cont-fb">
                            <img src="imagenes/logofb.png"><a href="">Iniciar sesion con Facebook</a>
                        </div>
                        <div class="olv-cont">
                            <a href="">¿Has olvidado la contraseña?</a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="cont-registro">
                ¿No tienes una cuenta? <a href="registro.php">Regístrate</a>
            </div>
            <center>Descarga la aplicación.</center>
            <center><img src="imagenes/appstores1.png"></center>
        </div>
    </div>
</body>
</html>
<?php
ob_end_flush();
?>